﻿using MassTransit;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Text.Json;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using System.Linq;
using System.Collections.Generic;
using System;
using Pcf.SharedLib.Contracts;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class GivingToCustomerConsumer : IConsumer<PromoCodeNotificationContract>
    {
        private readonly ILogger<GivingToCustomerConsumer> logger;
        private readonly IRepository<PromoCode> promoCodesRepository;
        private readonly IRepository<Preference> preferencesRepository;
        private readonly IRepository<Customer> customersRepository;

        public GivingToCustomerConsumer(
            ILogger<GivingToCustomerConsumer> logger, 
            IRepository<PromoCode> promoCodesRepository,
            IRepository<Preference> preferencesRepository,
            IRepository<Customer> customersRepository)
        {
            this.logger = logger;
            this.promoCodesRepository = promoCodesRepository;
            this.preferencesRepository = preferencesRepository;
            this.customersRepository = customersRepository;
        }

        public async Task Consume(ConsumeContext<PromoCodeNotificationContract> context)
        {
            PromoCodeNotificationContract msg =  context.Message;
            
            string json = JsonSerializer.Serialize(msg);
            logger.LogInformation("Recieved message: " + json);

            var preference = await preferencesRepository.GetByIdAsync(msg.PreferenceId);

            if (preference != null)
            {
                var customers = await customersRepository.GetWhere(d => d.Preferences.Any(x => x.Preference.Id == preference.Id));

                if (customers.Any())
                {
                    PromoCode promoCode = MapPromoCode(msg, preference, customers);
                    await promoCodesRepository.AddAsync(promoCode);
                    logger.LogWarning($"Promocode {promoCode.Id} created.");
                }
                else 
                {
                    logger.LogWarning($"No customers with Preference id: {msg.PreferenceId}");
                }
            }
            else
            {
                logger.LogWarning($"Preference with id {msg.PreferenceId} was not registered.");
            }           
        }

        private PromoCode MapPromoCode(PromoCodeNotificationContract msg, Preference preference, IEnumerable<Customer> customers)
        {
            var promocode = new PromoCode();
            promocode.Id = msg.PromoCodeId;

            promocode.PartnerId = msg.PartnerId;
            promocode.Code = msg.PromoCode;
            promocode.ServiceInfo = msg.ServiceInfo;

            promocode.BeginDate = DateTime.Parse(msg.BeginDate);
            promocode.EndDate = DateTime.Parse(msg.EndDate);

            promocode.Preference = preference;
            promocode.PreferenceId = preference.Id;

            promocode.Customers = new List<PromoCodeCustomer>();

            foreach (var item in customers)
            {
                promocode.Customers.Add(new PromoCodeCustomer()
                {

                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = promocode.Id,
                    PromoCode = promocode
                });
            };
            return promocode;
        }
    }
}
