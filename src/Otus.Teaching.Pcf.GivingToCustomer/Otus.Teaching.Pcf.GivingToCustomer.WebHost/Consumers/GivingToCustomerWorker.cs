﻿using MassTransit;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class GivingToCustomerWorker : BackgroundService
    {
        private readonly ILogger<GivingToCustomerWorker> logger;
        private readonly IBusControl massTransitBus;

        public GivingToCustomerWorker(ILogger<GivingToCustomerWorker> logger, IBusControl MassTransitBus)
        {
            this.logger = logger;
            massTransitBus = MassTransitBus;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("GivingToCustomerWorker is starting...");            
            return base.StartAsync(cancellationToken);            
        }


        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            logger.LogInformation("GivingToCustomerWorker is running...");
            return massTransitBus.StartAsync(stoppingToken);            
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {            
            massTransitBus.StopAsync(cancellationToken);
            logger.LogInformation("GivingToCustomerWorker stopped...");
            return base.StopAsync(cancellationToken);
        }

    }
}
