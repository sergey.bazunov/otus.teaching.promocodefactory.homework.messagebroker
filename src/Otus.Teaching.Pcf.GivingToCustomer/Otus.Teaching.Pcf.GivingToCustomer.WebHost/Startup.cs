using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Integration;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;
using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers;
using Pcf.SharedLib.Configs;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddMvcOptions(x=> 
                x.SuppressAsyncSuffixInActionNames = false);
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<INotificationGateway, NotificationGateway>();
            services.AddScoped<IDbInitializer, EfDbInitializer>();
            services.AddDbContext<DataContext>(x =>
            {
                //x.UseSqlite("Filename=PromocodeFactoryGivingToCustomerDb.sqlite");
                x.UseNpgsql(Configuration.GetConnectionString("PromocodeFactoryGivingToCustomerDb"));
                x.UseSnakeCaseNamingConvention();
                x.UseLazyLoadingProxies();
            });

            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory Giving To Customer API Doc";
                options.Version = "1.0";
            });

            var RabbitGatewaySettings = Configuration.GetSection("IntegrationRabbitGatewaySettings")
                .Get<IntegrationRabbitGatewaySettings>();

            int rabbitMqPort = RabbitGatewaySettings.RabbitPort;
            if (Environment.GetEnvironmentVariable("RABBIT_PORT") != null)
                rabbitMqPort = int.Parse(Environment.GetEnvironmentVariable("RABBIT_PORT"));

            string rabbitMqHost = Environment.GetEnvironmentVariable("RABBIT_HOST") ?? RabbitGatewaySettings.RabbitHost;
            string rabbitMqUser = Environment.GetEnvironmentVariable("RABBITMQ_USER") ?? RabbitGatewaySettings.RabbitUser;
            string rabbitMqPass = Environment.GetEnvironmentVariable("RABBITMQ_PASS") ?? RabbitGatewaySettings.RabbitPass;

            services.AddMassTransit(configurator =>
            {
                configurator.AddConsumer<GivingToCustomerConsumer>();
                configurator.UsingRabbitMq((context, rabbitConfigurator) =>
                {
                    rabbitConfigurator.Host(rabbitMqHost, (ushort)rabbitMqPort, "/",
                        hostConfigurator =>
                        {
                            hostConfigurator.Username(rabbitMqUser);
                            hostConfigurator.Password(rabbitMqPass);
                        }
                    );
                    rabbitConfigurator.AutoStart = false; // start when IBusControl injects
                    rabbitConfigurator.ConfigureEndpoints(context);
                });
            });

            services.AddHostedService<GivingToCustomerWorker>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            
            dbInitializer.InitializeDb();
        }
    }
}