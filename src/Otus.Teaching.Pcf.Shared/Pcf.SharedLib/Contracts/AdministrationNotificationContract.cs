﻿using System;

namespace Pcf.SharedLib.Contracts
{
    public class AdministrationNotificationContract
    {
        public Guid partnerManagerId { get; set; }
    }
}
