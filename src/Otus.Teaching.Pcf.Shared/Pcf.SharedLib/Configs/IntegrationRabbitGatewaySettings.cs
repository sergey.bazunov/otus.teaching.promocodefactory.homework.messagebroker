﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pcf.SharedLib.Configs
{
    public class IntegrationRabbitGatewaySettings
    {
        public string RabbitHost { get; set; }
        public int RabbitPort { get; set; }
        public string RabbitUser { get; set; }
        public string RabbitPass { get; set; }
    }
}
