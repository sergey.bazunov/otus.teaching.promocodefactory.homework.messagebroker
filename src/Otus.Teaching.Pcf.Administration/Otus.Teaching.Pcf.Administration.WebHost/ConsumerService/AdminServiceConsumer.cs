﻿using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Pcf.SharedLib.Contracts;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.ConsumerService
{
    public class AdminServiceConsumer : IConsumer<AdministrationNotificationContract>
    {
        private readonly ILogger<AdminServiceConsumer> logger;
        private readonly IRepository<Employee> employeeRepository;

        public AdminServiceConsumer(ILogger<AdminServiceConsumer> logger, IRepository<Employee> employeeRepository)
        {
            this.logger = logger;
            this.employeeRepository = employeeRepository;
        }

        public async Task Consume(ConsumeContext<AdministrationNotificationContract> context)
        {
            logger.LogInformation("Recieved message: " + context.Message.partnerManagerId.ToString());
            
            Guid employeeId = context.Message.partnerManagerId;

            var employee = await employeeRepository.GetByIdAsync(employeeId);

            if (employee != null)
            {
                employee.AppliedPromocodesCount++;
                await employeeRepository.UpdateAsync(employee);

                logger.LogInformation($"Employee {employeeId} updated.");
            }
            else 
            {
                logger.LogWarning($"Employee {employeeId} not found.");
            }
        }
    }
}