﻿using MassTransit;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.ConsumerService
{
    public class AdministrationWorker : BackgroundService
    {
        private readonly ILogger<AdministrationWorker> logger;
        private readonly IBusControl massTransitBus;

        public AdministrationWorker(ILogger<AdministrationWorker> logger, IBusControl MassTransitBus)
        {
            this.logger = logger;
            massTransitBus = MassTransitBus;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("AdministrationConsumerService is Started...");
            return base.StartAsync(cancellationToken);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            logger.LogInformation("AdministrationConsumerService is running...");
            return massTransitBus.StartAsync(stoppingToken);
        }


        public override Task StopAsync(CancellationToken cancellationToken)
        {
            massTransitBus.StopAsync(cancellationToken);
            return base.StopAsync(cancellationToken);
        }
    }
}
