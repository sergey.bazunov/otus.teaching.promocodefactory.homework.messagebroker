﻿using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Pcf.SharedLib.Contracts;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.RabbitGateway
{
    public class GivingPromoCodeToCustomerRabbitGateway : IGivingPromoCodeToCustomerGateway
    {
        private readonly IBus massTrasitBus;
        private readonly ILogger<GivingPromoCodeToCustomerRabbitGateway> _logger;

        public GivingPromoCodeToCustomerRabbitGateway(IBus MassTrasitBus , ILogger<GivingPromoCodeToCustomerRabbitGateway> logger)
        {
            this.massTrasitBus = MassTrasitBus;
            _logger = logger;
        }

        public Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            var dto = new PromoCodeNotificationContract()
            {
                PartnerId = promoCode.PartnerId,
                PartnerManagerId = promoCode.PartnerManagerId,
                PreferenceId = promoCode.PreferenceId,
                PromoCodeId = promoCode.Id,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString()
            };

            _logger.LogInformation("Sending PromoCodeNotificationContract...");
            return massTrasitBus.Publish<PromoCodeNotificationContract>(dto);
        }
    }
}
