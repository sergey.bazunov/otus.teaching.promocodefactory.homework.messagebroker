﻿using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Pcf.SharedLib.Contracts;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.RabbitGateway
{
    public class AdministrationRabbitGateway : IAdministrationGateway
    {
        private readonly ILogger<AdministrationRabbitGateway> logger;

        public IBus massTrasitBus { get; }

        public AdministrationRabbitGateway(IBus MassTrasitBus, ILogger<AdministrationRabbitGateway> logger)
        {
            this.massTrasitBus = MassTrasitBus;
            this.logger = logger;
        }

        public Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            var msg = new AdministrationNotificationContract()
            {
                partnerManagerId = partnerManagerId
            };

            logger.LogInformation("Sending AdministrationNotificationContract");
            return massTrasitBus.Publish<AdministrationNotificationContract>(msg);
        }
    }
}
